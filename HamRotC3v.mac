/*                                                                   */
/*-------------------------------------------------------------------*/
/* Rotational molecular Hamiltonian operators in tensorial formalism */
/* C3v symmetric top version                                         */
/*-------------------------------------------------------------------*/
/*                                                                   */
/* Full expansion in Jx, Jy and Jz of operators                      */
/* Conversion formulas between tensorial and Watsonian parameters    */
/*                                                                   */
/* V. Boudon (ICB) - May 2021                                        */
/*                                                                   */

/*                                   */
/* Technical remarks:                */
/* Non-commutative multiplication: . */
/* Non-commutative exponent: ^^      */
/*                                   */

/*                 */
/* Initializations */
/*                 */
kill(all)$
load("clebsch_gordan")$
load("/Users/vboudon/Documents/Maxima/OpRot/angular")$
ħ:1$

/*                                       */
/* Limits                                */
/* Change maximum Omega value here       */
/* Present version handles Omega up to 8 */
/*                                       */
Omax:8$

/*                                             */
/* Definitions                                 */
/* J = [Jx,Jy,Jz]                              */
/* R Omega(K)m = R[Omega+1][index of K][K+1+m] */
/* R[2,1,m] is the elementary operator R1(1)m  */
/*                                             */
array(J,3)$
J[1]:Jx$
J[2]:Jy$
J[3]:Jz$
J2:sum(J[i]^^2,i,1,3)$
J4:expand(J2^^2)$
J6:expand(J2^^3)$
J8:expand(J2^^4)$
Jz2:J[3]^^2$
Jz4:J[3]^^4$
Jz6:J[3]^^6$
Jz8:J[3]^^8$
Jp:J[1]+%i*J[2]$
Jm:J[1]-%i*J[2]$
Jp3:expand(Jp^^3)$
Jm3:expand(Jm^^3)$
Jp6:expand(Jp^^6)$
Jm6:expand(Jm^^6)$
Jm1:1/sqrt(2)*Jp$
Jp0:J[3]$
Jp1:-1/sqrt(2)*Jm$
array(R,Omax+1,floor(Omax/2)+1,2*Omax+1)$
R[2,1,1]:2*Jm1$
R[2,1,2]:2*Jp0$
R[2,1,3]:2*Jp1$

/*                                 */
/* First degree 2 operator: R2(0)0 */
/*                                 */
R[3,1,1]:expand(sum(wigner_3j(1,1,0,m1,-m1,0)
                    *R[2,1,m1+2].R[2,1,2-m1],m1,-1,1))$

/*                        */
/* R Omega-K(0) operators */
/*                        */
for OmK:0 thru Omax step 2 do
   (R[OmK+1,1,1]:expand(R[3,1,1]^^(OmK/2))
   )$

/*                 */
/* RK(K) operators */
/*                 */
for K:2 thru Omax do
   (iK:floor(K/2)+1,
    iKm1:floor((K-1)/2+1),
    for m:-K thru K do
      (R[K+1,iK,K+m+1]:expand(sqrt(2*K+1)*sum(sum((-1)^(K-m)
       *wigner_3j(K-1,1,K,m1,m2,-m)
       *R[K,iKm1,m1+K].R[2,1,m2+2],m1,-(K-1),(K-1)),m2,-1,1))
      )
   )$

/*                                       */
/* R Omega(K)m operators for K < Omega   */
/* starting at R 3(1)m                   */
/*                                       */
for O:3 thru Omax do
  (for K:mod(O,2) thru O-2 step 2 do
    (iKK:floor(K/2)+1,
     iK:(K-mod(O,2))/2+1,
     for m:-K thru K do
     (R[O+1,iK,K+m+1]:expand(expand(R[3,1,1]^^((O-K)/2)).R[K+1,iKK,K+m+1])
      )
    )
  )$

/*                                              */
/* Sum of Hrot operators for C3v symmetric tops */
/* W: Wang matrix                               */
/* V: C3v symmetrization                        */
/* t: parameters                                */
/* HT: full Hamiltonian                         */
/*                                              */
it:0$
HT:0$
for O:2 thru Omax step 2 do
  (for K:0 thru O step 2 do
    (EM:K,
     ST:3,
	 if(O>6)then EM:0,
     for M:0 thru EM step ST do
      (it:it+1,
       op:0,
       if(M=0) then
         (st:1,
          W:1
         )
         else
         (st:2*M,
          W:1/sqrt(2)
         ),
       for m:-M thru M step st do
        (iK:(K-mod(O,2))/2+1,
         V:1,
         WW:W,
         if(m<0) then WW:(-1)^M*W,
         op:expand(op+WW*V*R[O+1,iK,K+m+1])
        ),
       b:1,
       if(K=0) then b:(-sqrt(3)/4)^(O/2),
       HT:expand(HT+t[it]*b*op)
      )
    )
  )$
tlist:makelist(t[i],i,1,it);

/*                                                                                            */
/* Watsonian                                                                                  */
/* From Papousek & Aliev's book and W. Jerzembeck et al., J. Mol. Spectrosc. 226 (2004) 24-31 */
/*                                                                                            */
JZpm3:expand(Jz.(Jp3+Jm3)+(Jp3+Jm3).Jz)$
HW:expand(B*J2+(A-B)*Jz2
          -D[J]*J4-D[JK]*J2.Jz2-D[K]*Jz4
          +epsilon*JZpm3+epsilon[J]*(J2.JZpm3)+epsilon[K]*(Jz2.JZpm3+JZpm3.Jz2)/2
          +H[J]*J6+H[JK]*J4.Jz2+H[KJ]*J2.Jz4+H[K]*Jz6
          +h[3]*(Jp6+Jm6)
		  +L[JJJJ]*J8+L[JJJK]*J6.Jz2+L[JJKK]*J4.Jz4+L[JKKK]*J2.Jz6+L[KKKK]*Jz8
         )$
twlist:[A,B,
        D[J],D[K],D[JK],epsilon,
        H[J],H[JK],H[KJ],H[K],epsilon[J],epsilon[K],h[3],
		L[JJJJ],L[JJJK],L[JJKK],L[JKKK],L[KKKK]];

/*                                         */
/* Term reordering to get a standard form: */
/* Jx^^l.Jy^^m.Jz^^n                       */
/* Barton Willis' code used here           */
/*                                         */
HWt:0$
for ip:1 thru it do
  (tw:coeff(HW,twlist[ip]),
   two:operator_apply(tw,ψ),
   twt:dot_form(two,ψ),
   opsubst:false,
   HWt:HWt+expand(twt)*twlist[ip]
  )$

HWt:expand(HWt)$

HTt:0$
for ip:1 thru it do
  (tt:coeff(HT,tlist[ip]),
   tto:operator_apply(tt,ψ),
   ttt:dot_form(tto,ψ),
   opsubst:false,
   HTt:HTt+expand(ttt)*tlist[ip]
  )$

HTt:expand(HTt)$

/*                                                           */
/* Extract terms:                                            */
/* For each degree f, consider terms Ja.Jb.Jc. ... (f terms) */
/* where a,b,c, ... are x, y or z,                           */
/* this amounts to count up to 3^f-1 in basis 3.             */
/* Then find the coefficient of each term in H.              */
/*                                                           */
ie:0$
for f:2 thru Omax step 2 do
  (print("Degree: ",f),
   for i:0 thru 3^f-1 do
     (p:i,
      T:1,
      for d:f thru 1 step -1 do
        (n:floor(p/3^(d-1)),
         p:p-n*3^(d-1),
         T:T.J[n+1]
        ),
        c:coeff(HTt,T),
        if(c#0) then
          (ie:ie+1,
           cw[ie]:coeff(HWt,T),
           ct[ie]:expand(rootscontract(radcan(c))),
           print("Term:        ",T),
           print("Coefficients: ",ct[ie],",",cw[ie])
          )
     )
  )$

/*              */
/* Solve system */
/*              */
eqlist:expand(radcan(makelist(ct[i]=cw[i],i,1,ie)))$
sol1:rootscontract(expand(linsolve(eqlist,tlist)))$
print("Watson -> Tensorial:")$
for ip:1 thru it do
  (print(sol1[ip])
  );
sol2:rootscontract(expand(linsolve(eqlist,twlist)))$
print("Tensorial -> Watson:")$
for ip:1 thru it do
  (print(sol2[ip])
  );
  
/*            */
/* TeX ouptut */
/*            */

with_stdout("/Users/vboudon/Documents/Maxima/OpRot/tex/HrotFormulasC3v_WtoT.tex",
  for ip:1 thru it do
    (tex(sol1[ip])
    )
)$

with_stdout("/Users/vboudon/Documents/Maxima/OpRot/tex/HrotFormulasC3v_TtoW.tex",
  for ip:1 thru it do
    (tex(sol2[ip])
    )
)$

/*                */
/* Save solutions */
/*                */

with_stdout("/Users/vboudon/Documents/Maxima/OpRot/HrotFormulasC3v.mac",
   grind(sol1),
   grind(sol2)
)$

/*                                                                       */
/* CH3I test                                                             */
/* Haykal et al., J. Quant. Spectrosc. Radiat. Transfer 173 (2016) 13-19 */
/* Result in GHz                                                         */
/*                                                                       */
print("CH3I:")$
tfit:[1.85810418,9.846364086E-1,-1.886317E-5,6.938426E-6,-2.618935609E-6,0.0,1.822E-10,3.281E-11,-1.823E-11,0.0,3.33E-12,0.0,0.0,0.0,0.0,0.0,0.0,0.0]$

c:299792458.0$
cghz:c/1E7$

tsubst:makelist(tlist[ip]=tfit[ip]*cghz,ip,1,it)$

for ip:1 thru it do
  (print(float(subst(tsubst,sol2[ip])))
  );

/*                    */
/* Trioxane test      */
/* From PGopher files */
/* Result in cm-1     */
/*                    */
print("Trioxane:")$

cmhz:c/1E4$

twfit:[2933.95,5273.25718,0.0013438797,-0.002016295,.00017,0.0,4.9061e-10,-2.0978e-9,2.7408e-9,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]$

twsubst:makelist(twlist[ip]=twfit[ip]/cmhz,ip,1,it)$

for ip:1 thru it do
  (print(float(subst(twsubst,sol1[ip])))
  );
