Maxima codes to:
- Derive analytical expressions of rotational operators in tensorial formalism.
- Derive parameter conversion formulas between tensorial and Watsonian formalisms.
V. Boudon (ICB) - April 2022 - Under license GPLv

See:
V. Boudon, C. Richard, M. Loete, B. Willis, J. Mol. Spectrosc. 385 (2022) 111602
https://doi.org/10.1016/j.jms.2022.111602

Programs (plaase see detailed comments in each file):

- OpRot.mac:
The main program to calculate tensorial rotational operators, including some graphical examples.

- OpRot_Load.mac:
A version that loads the operators calculated by OpRot.mac and performs the examples.

- angular.mac:
A companion program by B. Willis to reorder terms in standard form.

- HamRotC2v.mac:
Derives conversion formulas between tensorial and Watsonian formalisms for C2v-symmetry molecules.

- HamRotC3v.mac:
Derives conversion formulas between tensorial and Watsonian formalisms for C3v-symmetry molecules.
A1 vibrational state version.

- HrotFormulasC3vE.mac:
Derives conversion formulas between tensorial and Watsonian formalisms for C3v-symmetry molecules.
E vibrational state version.

- OpRotTd.mac:
Rotational operators symmetrized in the Td point group.

- Test_Hrot.mac:
Graphical semiclassical tests for the rotational Hamiltonian in Td symmetry.
