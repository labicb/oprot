/*                                                                   */
/*-------------------------------------------------------------------*/
/* Rotational molecular Hamiltonian operators in tensorial formalism */
/* Td spherical top version                                          */
/*-------------------------------------------------------------------*/
/*                                                                   */
/* Full expansion in Jx, Jy and Jz of operators symmetrized in O(3)  */
/* Storage of these operators in file "R"                            */
/* Examples of Td symmetrizatio + tests of semi-classical plots      */
/*                                                                   */
/* V. Boudon (ICB) - May 2021                                        */
/*                                                                   */

/*                                   */
/* Technical remarks:                */
/* Non-commutative multiplication: . */
/* Non-commutative exponent: ^^      */
/*                                   */

/*                                   */
/* Initializations and libraries     */
/*                                   */
kill(all)$
load("clebsch_gordan")$
load("draw")$

/*                      */
/* Limits and constants */
/*                      */
Omax:8$         /* Maximum value for Omega                    */
t:10^(-4)$      /* R66A1 coefficient for classical plots      */
Jmax:100$       /* Maximum J value for classical curve plots  */
JSmin:10$       /* Minimum J value for classical surface plot */
JSmax:80$       /* Maximum J value for classical surface plot */
JSstp:2$        /* J value step for classical surface plot    */
C:5$            /* Constant shift for classical surface plot  */
grsize:200$     /* Grid size for classical surface plot       */
imsz:800$       /* Image size for classical surface plot      */

/*                                             */
/* Definitions                                 */
/* J = [Jx,Jy,Jz]                              */
/* R Omega(K)m = R[Omega+1][index of K][K+1+m] */
/* R[2,1,m] is the elementary operator R1(1)m  */
/*                                             */
array(J,3)$
J[1]:Jx$
J[2]:Jy$
J[3]:Jz$
Jp:J[1]+%i*J[2]$
Jm:J[1]-%i*J[2]$
Jm1:1/sqrt(2)*Jp$
Jp0:J[3]$
Jp1:-1/sqrt(2)*Jm$
array(R,Omax+1,floor(Omax/2)+1,2*Omax+1)$
R[2,1,1]:2*Jm1$
R[2,1,2]:2*Jp0$
R[2,1,3]:2*Jp1$

/*                                 */
/* First degree 2 operator: R2(0)0 */
/*                                 */
R[3,1,1]:expand(sum(wigner_3j(1,1,0,m1,-m1,0)
                    *R[2,1,m1+2].R[2,1,2-m1],m1,-1,1))$

/*                        */
/* R Omega-K(0) operators */
/*                        */
for OmK:0 thru Omax step 2 do
   (R[OmK+1,1,1]:expand(R[3,1,1]^^(OmK/2))
   )$

/*                 */
/* RK(K) operators */
/*                 */
for K:2 thru Omax do
  (iK:floor(K/2)+1,
   iKm1:floor((K-1)/2+1),
   for m:-K thru K do
     (R[K+1,iK,K+m+1]:expand(sqrt(2*K+1)*sum(sum((-1)^(K-m)
      *wigner_3j(K-1,1,K,m1,m2,-m)
	  *R[K,iKm1,m1+K].R[2,1,m2+2],m1,-(K-1),(K-1)),m2,-1,1))
     )
  )$

/*                                       */
/* R Omega(K)m operators for K < Omega   */
/* starting at R 3(1)m                   */
/*                                       */
for O:3 thru Omax do
  (for K:mod(O,2) thru O-2 step 2 do
    (iKK:floor(K/2)+1,
     iK:(K-mod(O,2))/2+1,
     for m:-K thru K do
     (R[O+1,iK,K+m+1]:expand(expand(R[3,1,1]^^((O-K)/2)).R[K+1,iKK,K+m+1])
      )
    )
  )$

/*                    */
/* Saving R operators */
/*                    */
save("/Users/vboudon/Documents/Maxima/OpRot/R",R)$

/*                    */
/* ================== */
/*    APPLICATIONS    */
/* ================== */
/*                    */

/*                                          */
/* Some examples of Td symetrized operators */
/*                                          */
R22E1:R[3,2,3]$
R22E2:(R[3,2,1]+R[3,2,5])/sqrt(2)$
R22F2x:expand(-%i*(R[3,2,2]+R[3,2,4])/sqrt(2))$
R22F2y:expand((R[3,2,2]-R[3,2,4])/sqrt(2))$
R22F2z:expand(-%i*(R[3,2,1]-R[3,2,5])/sqrt(2))$
R33A2:expand(-%i*(R[4,2,2]-R[4,2,6])/sqrt(2))$
R33F1x:expand(sqrt(5)/4*(R[4,2,1]-R[4,2,7])
              -sqrt(3)/4*(R[3,2,3]-R[3,2,5]))$
R33F1y:expand(%i*sqrt(5)/4*(R[4,2,1]+R[4,2,7])
              +%i*sqrt(3)/4*(R[3,2,3]+R[3,2,5]))$
R33F1z:R[4,2,4]$
R31F1x:expand(1/sqrt(2)*(R[4,1,1]-R[4,1,3]))$
R31F1yx:expand(-%i/sqrt(2)*(R[4,1,1]+R[4,1,3]))$
R31F1z:expand(R[4,1,2])$
R44A1:expand(sqrt(5/24)*(R[5,3,1]+R[5,3,9])+sqrt(7/12)*R[5,3,5])$
R66A1:expand(-1/(2*sqrt(2))*R[7,4,7]+sqrt(7)/4*(R[7,4,3]+R[7,4,11]))$

/*                                                            */
/* Classical curve test                                       */
/* for R44A1 + t*R66A1                                        */
/* Hq: quantum Haliltonian, divided by 1E8                    */
/* Hc: classical Hamiltonian, switched to commutative algebra */
/*                                                            */
Hq:(R44A1+t*R66A1)/1E8$
Hc:float(expand(subst(["."="*","^^"="^"],Hq)))$
J0:sqrt(Jr*(Jr+1))$
eqJx:Jx=J0*sin(theta)*cos(phi)$
eqJy:Jy=J0*sin(theta)*sin(phi)$
eqJz:Jz=J0*cos(theta)$
eqt4:theta=0$
eqp4:phi=0$
C4:subst([eqJx,eqJy,eqJz,eqt4,eqp4],Hc)$
eqt3:theta=acos(1/sqrt(3))$
eqp3:phi=%pi/4$
C3:subst([eqJx,eqJy,eqJz,eqt3,eqp3],Hc)$
eqt2:theta=%pi/2$
eqp2:phi=%pi/4$
C2:subst([eqJx,eqJy,eqJz,eqt2,eqp2],Hc)$

/*                           */
/* Loading quantum levels    */
/*                           */
levels:read_matrix(file_search("/Users/vboudon/Documents/Maxima/OpRot/TestGS/jener.xy"))$
l:length(levels)$
lx:makelist(levels[i,1],i,1,l)$
ly:makelist(levels[i,2],i,1,l)$

/*                           */
/* Graphics: 2D energy lines */
/*                           */
set_draw_defaults(xlabel="J",
                  ylabel="Energy / 10^8 / arbitrary unit",
				  xrange=[-0.5,Jmax+0.5],
                  title=sconcat("R^{4(4,A_{1})} + ",t," R^{6(6,A_{1})}: quantum and classical energy levels "),
				  font="Times-Roman",
				  font_size=12,
				  grid=true,
				  dimensions=[1500,1000],
				  user_preamble="set key bottom left",
				  terminal=pdf,
				  file_name="/Users/vboudon/Documents/Maxima/OpRot/Figures/HA1_curves"
				 )$
draw2d(color=gray,point_type=circle,point_size=0.2,key="Quantum",points(lx,ly),
	   color=red,key="C_4",explicit(C4,Jr,0,Jmax),
	   color=green,key="C_3",explicit(C3,Jr,0,Jmax),
	   color=blue,key="C_2",explicit(C2,Jr,0,Jmax)
	  )$

/*                           */
/* Classical surface test    */
/* for C + R44A1 + t * R66A1 */
/* and for J = JS            */
/*                           */
for JS:JSmin thru JSmax step JSstp do
  (eqJ:Jr=JS,
   S:subst([eqJx,eqJy,eqJz,eqJ],Hc+C),
   /*                            */
   /* Graphics: 3D surface plots */
   /*                            */
   amax:max(subst([eqt2,eqp2],S),subst([eqt3,eqp3],S),subst([eqt4,eqp4],S)),
   set_draw_defaults(xlabel="E_x / 10^8 / arb. unit",
                     ylabel="E_y / 10^8 / arb. unit",
				     zlabel="E_z / 10^8 / arb. unit",
  		  		     xrange=[-amax,amax],
				     yrange=[-amax,amax],
				     zrange=[-amax,amax],
				     xyplane=-amax,
				     grid=true,
		             view=[70,20],
		             surface_hide=true,
                     title=sconcat(C," 10^8 + R^{4(4,A_{1})} + ",t," R^{6(6,A_{1})}: classical surface for J = ",JS),
				     font="Times-Roman",
				     font_size=12,
				     dimensions=[imsz,imsz],
				     terminal=png,
				     file_name=sconcat("/Users/vboudon/Documents/Maxima/OpRot/Images/HA1_surface_J",JS)
				    ),
   draw3d(xu_grid=grsize,
          yv_grid=grsize/2,
	      enhanced3d=[S,phi,theta],
	      spherical(S,phi,0,2*%pi,theta,0,%pi)
	     )
  )$
	  
