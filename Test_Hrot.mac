/*                                                                  */
/*------------------------------------------------------------------*/
/* Symmetrized rotational operators in the Td point group: example  */
/* Test of semi-classical plots                                     */
/*------------------------------------------------------------------*/
/*                                                                  */
/* Full expansion in Jx, Jy and Jz                                  */
/* Reading these operators from file "RT_to_8_unordered"            */
/*                                                                  */
/* V. Boudon (ICB) - May 2022                                       */
/*                                                                  */

/*                                   */
/* Initializations and libraries     */
/*                                   */
kill(all)$
load("draw")$
load("/Users/vboudon/Documents/Maxima/OpRot/RT_to_8_unordered")$
                                                      /* Load Td R operators calculated by OpRotTd.mac */

/*                      */
/* Limits and constants */
/*                      */
Omax:8$         /* Maximum value for Omega                    */
Jmax:100$       /* Maximum J value for classical curve plots  */
JS:50$          /* J value for 3D surface plot                */
C:0.2$          /* Constant shift for classical surface plot  */
grsize:200$     /* Grid size for classical surface plot       */
imsz:2000$      /* Image size for classical surface plot      */

/*                           */
/* Loading quantum levels    */
/*                           */
levels:read_matrix(file_search("/Users/vboudon/Documents/Maxima/OpRot/TestGS/jener.xy"))$
l:length(levels)$
lx:makelist(levels[i,1],i,1,l)$
ly:makelist(levels[i,2],i,1,l)$

/*                           */
/* Effective Hamiltonian     */
/*                           */

t: [ 0.13778054572E+00,
    -0.41380253094E-07,
    -0.33605104766E-08,
    -0.21026176312E-13,
     0.21485549711E-14,
     0.35386683028E-15,
     0.10153837427E-16,
     0.11571819224E-18,
     0.36235946947E-19,
    -0.54404352192E-19]$

Hq:t[3]*RT[5,3,1,1,1]+
   t[5]*RT[7,3,1,1,1]+
   t[6]*RT[7,4,1,1,1]+
   t[8]*RT[9,3,1,1,1]+
   t[9]*RT[9,4,1,1,1]+
   t[10]*RT[9,5,1,1,1]$

/*                                                            */
/* Classical curve test                                       */
/* Hq: quantum Haliltonian, divided by 1E8                    */
/* Hc: classical Hamiltonian, switched to commutative algebra */
/*                                                            */
Hc:expand(subst(["."="*","^^"="^"],Hq))$
J0:sqrt(Jr*(Jr+1))$
eqJx:Jx=J0*sin(theta)*cos(phi)$
eqJy:Jy=J0*sin(theta)*sin(phi)$
eqJz:Jz=J0*cos(theta)$
eqt4:theta=0$
eqp4:phi=0$
C4:float(subst([eqJx,eqJy,eqJz,eqt4,eqp4],Hc))$
eqt3:theta=acos(1/sqrt(3))$
eqp3:phi=%pi/4$
C3:float(subst([eqJx,eqJy,eqJz,eqt3,eqp3],Hc))$
eqt2:theta=%pi/2$
eqp2:phi=%pi/4$
C2:float(subst([eqJx,eqJy,eqJz,eqt2,eqp2],Hc))$

/*                           */
/* Graphics: 2D energy lines */
/*                           */
set_draw_defaults(xlabel="$J$",
                  ylabel="Reduced wavenumber / cm$^{-1}$",
				  xrange=[-0.5,Jmax+0.5],
				  yrange=[-2,1.5],
                  title=sconcat("$H_{\\mathrm{rot}}$: Quantum and classical energy levels"),
				  font="Times-Roman",
				  font_size=12,
				  grid=true,
				  dimensions=[1500,1000],
				  user_preamble="set key bottom left",
				  terminal=epslatex_standalone,
				  file_name="/Users/vboudon/Documents/Maxima/OpRot/Figures/Hrot_curves"
				 )$
draw2d(color=gray,point_type=circle,point_size=0.5,key="Quantum",points(lx,ly),
	   color=red,key="$C_4$",explicit(C4,Jr,0,Jmax),
	   color=green,key="$C_3$",explicit(C3,Jr,0,Jmax),
	   color=blue,key="$C_2$",explicit(C2,Jr,0,Jmax)
	  )$

/*                           */
/* Classical surface test    */
/* for J = JS                */
/*                           */
eqJ:Jr=JS$
S:subst([eqJx,eqJy,eqJz,eqJ],bfloat(Hc+C))$
amax:max(subst([eqt2,eqp2],S),subst([eqt3,eqp3],S),subst([eqt4,eqp4],S))$
set_draw_defaults(xlabel="\\vspace*{-15mm}$\\widetilde{\\nu}_x/$cm$^{-1}$",
                  ylabel="\\hspace*{15mm}$\\widetilde{\\nu}_y/$cm$^{-1}$",
			      zlabel="$\\widetilde{\\nu}_z/$cm$^{-1}$",
		  		  xrange=[-amax,amax],
			      yrange=[-amax,amax],
			      zrange=[-amax,amax],
			      xyplane=-amax,
			      grid=true,
	              view=[60,20],
	              surface_hide=true,
                  title=sconcat("\\LARGE Reduced wavenumber $\\widetilde{\\nu}+",C,"$ cm$^{-1}$, $J=",JS,"$"),
			      font="Times-Roman",
			      font_size=10,
			      dimensions=[imsz,imsz],
			      terminal=epslatex_standalone,
			      file_name=sconcat("/Users/vboudon/Documents/Maxima/OpRot/Figures/HA1_surface_J",JS)
			     )$
draw3d(xu_grid=grsize,
       yv_grid=grsize/2,
       enhanced3d=[S,phi,theta],
       spherical(S,phi,0,2*%pi,theta,0,%pi),
       contour_levels = 50,
       contour = base
      )$
